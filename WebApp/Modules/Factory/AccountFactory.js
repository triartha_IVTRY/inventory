﻿
//services
InventoryApp.factory('userService', ['$rootScope',function ($rootScope) {
    var fac = {};
    fac.CurrentUser = null;
    fac.SetCurrentUser = function (user) {
        fac.CurrentUser = user;
        localStorage.user = angular.toJson(user);
    }
    fac.GetCurrentUser = function () {
        fac.CurrentUser = angular.fromJson(localStorage.user);
        return fac.CurrentUser;
    }

    //$rootScope.$apply();
    return fac;
}])


InventoryApp.factory('accountService', ['$http', '$q', 'serviceBasePath', 'AuthServerBasePath', 'userService', 'authData', 'CrudService', 'UtilityService',
    function ($http, $q, serviceBasePath, AuthServerBasePath, userService, authData, CrudService, UtilityService) {
        var fac = {};
        var baseUrl = AuthServerBasePath + '/api/Auth';
        //localStorage.client_ID = '';
        //localStorage.client_Secret = '';

        var apiRoute = baseUrl + "/GetClientID";
        if (localStorage.client_ID == '' || localStorage.client_ID === 'undefined' || localStorage.client_ID == null) {
            var promise = CrudService.getAll(apiRoute);
            promise.then(function (response) {
                localStorage.client_ID = 'B2BM_'+response.data.client_ID;
                localStorage.client_Secret = response.data.client_Secret;
            },
            function (error) {
                localStorage.client_ID = '';
                localStorage.client_Secret = '';
                console.log("Fail to get ClientID: " + error);
            });
        }

        fac.login = function (user) {
            //var obj = { 'username': user.username, 'password': user.password, 'grant_type': 'password' };
            var obj = {
                'username': user.username, 'password': user.password, 'grant_type': 'password',
                'client_id': localStorage.client_ID, 'client_secret': localStorage.client_Secret
            };
  
            Object.toparams = function ObjectsToParams(obj) {
                var p = [];
                for (var key in obj) {
                    p.push(key + '=' + encodeURIComponent(obj[key]));
                }
                return p.join('&');
            }

            var defer = $q.defer();
            $http({
                method: 'post',
                url: AuthServerBasePath + "/token",
                data: Object.toparams(obj),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                userService.SetCurrentUser(response.data);
                defer.resolve(response.data);
                authData.authenticationData.IsAuthenticated = true;
                authData.authenticationData.userName = user.username;
                localStorage.userName = user.username;
                localStorage.IsAuthenticated = true;
                localStorage.fullName = authData.authenticationData.fullName;

                $http.get(AuthServerBasePath + '/api/Auth/GetUsers/' + user.username).then(function (response) {
                    var decData = UtilityService.AESDecrypt(response.data);
                    var myData = angular.fromJson(decData);
                                
                    authData.authenticationData.userGroup = myData.UserGroup;
                    authData.authenticationData.fullName = myData.UserFullName;
                    authData.authenticationData.FormatOrder = myData.FormatOrder; 
                    localStorage.fullName = authData.authenticationData.fullName;
                    localStorage.userName = user.username;
                });

            }, function (error) {                
                defer.reject(error);
            })
            return defer.promise;
        }

        fac.callRefreshToken = function () {
            var currentUser = userService.GetCurrentUser();
            var obj = {
                'refresh_token': currentUser.refresh_token, 'grant_type': 'refresh_token',
                'client_id': localStorage.client_ID, 'client_secret': localStorage.client_Secret
            };
            Object.toparams = function ObjectsToParams(obj) {
                var p = [];
                for (var key in obj) {
                    p.push(key + '=' + encodeURIComponent(obj[key]));
                }
                return p.join('&');
            }

            var defer = $q.defer();
            $http({
                method: 'post',
                url: AuthServerBasePath + "/token",
                data: Object.toparams(obj),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                defer.resolve(response.data);
                userService.SetCurrentUser(response.data);
            }, function (error) {
                console.error('error refresh token  = ' + JSON.stringify(error));
                defer.reject(error.data);
            })
            return defer.promise;
        }
        fac.logout = function () {
            userService.SetCurrentUser(null);
            authData.authenticationData.IsAuthenticated = false;
            authData.authenticationData.userName = "";

            // remove user from local storage and clear http auth header
            $http.defaults.headers.common['Authorization'] = 'Bearer ';
        }
        return fac;
}])

InventoryApp.factory('authData', [function () {
    var authDataFactory = {};
    var _authentication = {
        IsAuthenticated: false,
        userName: "",
        userGroup: 0,
        fullName: "",
        loginTime: "",
        FormatOrder: ""
    };

    authDataFactory.authenticationData = _authentication;
    authDataFactory.menitKe = function (_loginTime) { return getMinutesDiff(_loginTime); }
    authDataFactory.isLoginExpiredy = false;
    

    authDataFactory.isLoginExpired = 
        function () {
        var result = true;
        var minuteDiff = getMinutesDiff(localStorage.loginTime);
        if (minuteDiff > 5 && minuteDiff <= 15) {
            localStorage.loginTime = moment(new Date()).format('YYYY/MM/DD HH:mm:ss');
            result = false;
        } else
            if (minuteDiff <= 15) {
                result = false;
            }

        return result;
    }

    return authDataFactory;
}]);


InventoryApp.factory('dataService', ['$http', 'serviceBasePath', 'UtilityService', function ($http, serviceBasePath, UtilityService) {
    var fac = {};

    fac.StorageVar = null;
    fac.SetStorageVar = function (varName, theValue, isJson) {
        if (isJson) localStorage.setItem(varName, UtilityService.AESEncrypt(angular.toJson(theValue)));
        else localStorage.setItem(varName, UtilityService.AESEncrypt(theValue));
    }

    fac.GetStorageVar = function (varName, isJson) {
        var varValue = localStorage.getItem(varName)
        if (isJson) fac.StorageVar = JSON.parse(angular.fromJson(UtilityService.AESDecrypt(varValue)));
        else  fac.StorageVar = UtilityService.AESDecrypt(varValue);
        return fac.StorageVar;
    }

    //fac.GetAnonymousData = function () {
    //    return $http.get(serviceBasePath + '/api/data/forall').then(function (response) {
    //        return response.data;
    //    })
    //}

    //fac.GetAuthenticateData = function () {
    //    return $http.get(serviceBasePath + '/api/data/authenticate').then(function (response) {
    //        return response.data;
    //    })
    //}

    //fac.GetAuthorizeData = function () {
    //    return $http.get(serviceBasePath + '/api/data/authorize').then(function (response) {
    //        return response.data;
    //    })
    //}

    //fac.GetSignOut = function () {
    //    return $http.get(serviceBasePath + '/api/data/signout').then(function (response) {
    //        return response.data;
    //    })
    //}

    return fac;
}]);

InventoryApp.factory('persistObject', [function () {

    var persistObject = [];

    function set(objectName, data) {
        persistObject[objectName] = data;
    }
    function get(objectName) {
        return persistObject[objectName];
    }

    return {
        set: set,
        get: get
    }
}]);