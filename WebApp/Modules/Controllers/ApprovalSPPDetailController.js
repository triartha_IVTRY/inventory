﻿InventoryApp.controller('ApprovalSPPDetailController', ['$scope', '$rootScope', '$location', '$rootScope', '$routeParams', 'accountService', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService', 'uiGridConstants', 'uiGridGroupingConstants',
    function ($scope, $rootScope, $location, $rootScope, $routeParams, accountService, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService, uiGridConstants, uiGridGroupingConstants) {
    $scope.authentication = authData.authenticationData;

    var baseUrl = serviceBasePath + '/api/ApprovalSPPDetail';
       var vm = this;

       $scope.Title = 'SPP Detail';
       $scope.EditMode = false;
       $scope.AddMode = false;
       $rootScope.isLoading = true;
       $scope.mySelections = [];



       $scope.authentication = authData.authenticationData;
       if (!$scope.authentication.IsAuthenticated) {
           sessionStorage.returnUrl = $location.url();
       }

       $scope.emptyData = function () {
           $scope.DTA = {};
           $scope.DTARequestDetail = {};
       }
                
       $scope.Back2SPP = function () {
           $location.path('/ApprovalSPP');
       }

       $scope.getSPPDetail = function () {
           if ($routeParams.sppid != null && $routeParams.sppid != 'undefined') {
               var sppid = $routeParams.sppid;

               var apiRoute = baseUrl + '/GetSPPDetail/' + sppid;
               $rootScope.isLoading = true;
               var promise = CrudService.getAll(apiRoute);
               promise.then(function (response) {
                   var decData = UtilityService.AESDecrypt(response.data);
                   $scope.Data = angular.fromJson(decData);

                   $rootScope.isLoading = false;
                   $scope.checkbtnToSEM(sppid);
               },
               function (error) {
                   $rootScope.isLoading = false;
                   toastr.error('Can not get spp detail data !', 'Error');
                   if ($rootScope.isDebugMode)
                       console.error("Error: " + JSON.stringify(error));
               });
           }

       };
       $scope.getSPPDetail();

       $scope.checkbtnToSEM = function (sppid) {

           //$rootScope.UserID = 3;
           var apiRoute = baseUrl + '/CheckPOPEmail/?id=' + sppid + "&userid=" + $rootScope.UserID;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var emailpop = angular.fromJson(response.data);

               if (emailpop == 'POPEmailEnabled') {
                   $("#btnEmailToSEM").show();
                   $("#btnEmailToSEM").attr("disabled", false);

               } else if (emailpop == 'POPEmailVisible') {
                   $("#btnEmailToSEM").show();
                   $("#btnEmailToSEM").attr("disabled", true);
               } else {
                   $("#btnEmailToSEM").hide();
                   $("#btnEmailToSEM").attr("disabled", true);
               }
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get SPP data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       }


       $scope.ProgressToSEM = function () {
           var sppid = $routeParams.sppid;
           var apiRoute = baseUrl + '/EmailToSEM/' + sppid;
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute);
           promise.then(function (response) {
               toastr.success('Data posted successfully !');
               $scope.Back2SPP();
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Email to SEM failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       }

       $scope.DetailRequest = function (id) {
           var apiRoute = baseUrl + '/GetSPPRequestDetail/' + id;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.DataRequestDetail = angular.fromJson(decData);

               $("#reqid").val(id);
               for (i = 0; i < $scope.Data.length; i++) {
                   if ($scope.Data[i].RequestNo === id) {
                       $scope.DTA = $scope.Data[i];                       
                   }
               }

               $rootScope.isLoading = false;
               $("#detailrequestModal").modal("show");
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get request detail data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });

           
       }

       $scope.ApproveRequestPOP = function () {
           var id = $("#reqid").val();
           

           $scope.DTA.RequestDate = $scope.DataRequestDetail[0].RequestDate;
           $scope.DTA.Status = 1;
           $scope.DTA.RequestNo = id;
           //if ($scope.DTA.IsPOP == "1") {
           //    $scope.DTA.Status = 1;
           //} else if ($scope.DTA.IsSEM == "1") {
           //    $scope.DTA.Status = 2;
           //}

           Data = $scope.DTA;
           var EncData = UtilityService.AESEncrypt(Data);

           $rootScope.isLoading = true;
           var apiRoute = serviceBasePath + '/api/Request/PutRequest/' + id;
           var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));

           promise.then(function (response) {
               $("#detailrequestModal").modal("hide");
               $scope.getSPPDetail();
               toastr.success('POP Approved !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Approve SEM or POP failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       }



       $scope.ApproveRequestSEM = function () {
           var id = $("#reqid").val();

           $scope.DTA.RequestDate = $scope.DataRequestDetail[0].RequestDate;
           $scope.DTA.Status = 2;
           $scope.DTA.RequestNo = id;

           //if ($scope.DTA.IsPOP == "1") {
           //    $scope.DTA.Status = 1;
           //} else if ($scope.DTA.IsSEM == "1") {
           //    $scope.DTA.Status = 2;
           //}

           Data = $scope.DTA;
           var EncData = UtilityService.AESEncrypt(Data);

           $rootScope.isLoading = true;
           var apiRoute = serviceBasePath + '/api/Request/PutRequest/' + id;
           var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));

           promise.then(function (response) {
               $("#detailrequestModal").modal("hide");
               $scope.getSPPDetail();
               toastr.success('SEM Approved !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Approve SEM & POP failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       }

       $scope.RejectRequest = function () {
           var id = $("#reqid").val();
           $scope.DTA.RequestDate = $scope.DataRequestDetail[0].RequestDate;
           $scope.DTA.RequestNo = id;
           $scope.DTA.Status = 3;
           $scope.DTA.RejectDesc = $("#txtRejectionNotes").val();
           Data = $scope.DTA;
           var EncData = UtilityService.AESEncrypt(Data);

           $rootScope.isLoading = true;
           var apiRoute = serviceBasePath + '/api/Request/PutRequest/' + id;
           var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));

           promise.then(function (response) {
               $("#detailrequestModal").modal("hide");
               $scope.getSPPDetail();
               toastr.success('Data Success Rejected !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Reject SEM & POP failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       }

       $scope.gridOptions = {
           data: 'Data',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 20,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [
                 
                 { name: 'RequestNo', field: 'RequestNo', displayName: 'Request No', enableFiltering: true, enableSorting: true,  headerCellClass: 'text-center' },
                 { name: 'Status', field: 'StatusDesc', displayName: 'Status', enableFiltering: true, enableSorting: true, headerCellClass: 'text-center' },
                 {
                     name: 'Action',
                     headerCellClass: 'text-center',
                     cellTemplate: 'actButton.html'
                 }],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };

       $scope.gridDetailRequest = {
           data: 'DataRequestDetail',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 10,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [

                 { name: 'ItemName', field: 'ItemName', displayName: 'Item Name', width: '220', enableFiltering: true, enableSorting: true, headerCellClass: 'text-center' },
                 { name: 'RequestQty', field: 'RequestQty', displayName: 'Qty', width: '100', enableFiltering: true, enableSorting: true, headerCellClass: 'text-center' },
                 { name: 'RequestDesc', field: 'RequestDesc', displayName: 'Description', width: '530', enableFiltering: true, enableSorting: true, headerCellClass: 'text-center' }
           ],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };
   }])