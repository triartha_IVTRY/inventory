﻿InventoryApp.controller('POController', ['$scope', '$rootScope', 'accountService', '$location', '$rootScope', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService', 'uiGridConstants', 'uiGridGroupingConstants',
   function ($scope, $rootScope, accountService, $location, $rootScope, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService, uiGridConstants, uiGridGroupingConstants) {
       $scope.authentication = authData.authenticationData;

       var baseUrl = serviceBasePath + '/api/PO';
       var vm = this;

       //$rootScope.UserID = 1;
       //$scope.btnPrintShow = false;
       $scope.Title = 'Purchase Order';
       $scope.EditMode = false;
       $scope.AddMode = false;
       $scope.EditDetailMode = false;
       $scope.isUpdate = false;
       $rootScope.isLoading = true;
       $scope.mySelections = [];
       //$rootScope.SiteID = 2;
       var siteId = $rootScope.SiteID;

       $.datetimepicker.setLocale('en');
       $('#PODate').datetimepicker({
           yearOffset: 0,
           lang: 'ch',
           timepicker: false,
           startDate: new Date(),
           format: 'd/m/Y'
       });

       $.datetimepicker.setLocale('en');
       $('#PODeadline').datetimepicker({
           yearOffset: 0,
           lang: 'ch',
           timepicker: false,
           startDate: new Date(),
           format: 'd/m/Y'
       });

       $scope.authentication = authData.authenticationData;
       if (!$scope.authentication.IsAuthenticated) {
           sessionStorage.returnUrl = $location.url();
       }
       $scope.emptyData = function () {
           $scope.DTA = {};
       }

       $scope.SaveData = function () {        
           if ($scope.isUpdate) $scope.UpdateData();
           else $scope.SaveNewData();
           $scope.isUpdate = false;
       }

       $scope.SaveNewData = function () {

           var poDate;
           var poDateSplit = $('#PODate').val().split("/");
           poDate = new Date(Date.parse(poDateSplit[2] + "-" + poDateSplit[1] + "-" + poDateSplit[0])).toUTCString();

           if (poDate != "Invalid PO Date")
               $scope.DTA.PODate = poDate;
           else
               $scope.DTA.PODate = null;

           var dlDate;
           var dlDateSplit = $('#PODeadline').val().split("/");
           dlDate = new Date(Date.parse(dlDateSplit[2] + "-" + dlDateSplit[1] + "-" + dlDateSplit[0])).toUTCString();

           if (dlDate != "Invalid PO Deadline Date")
               $scope.DTA.PODeadline = dlDate;
           else
               $scope.DTA.PODeadline = null;
          
           $scope.ReturnHeaderData = {
               PONo: $scope.DTA.PONo,
               PODate: moment($scope.DTA.PODate),
               PODeadline: moment($scope.DTA.PODeadline),
               PONotes: $scope.DTA.PONotes,
               VendorID: $scope.DTA.VendorID,
               CreateBy: $rootScope.UserID,
               CreateTime: '',
               SiteID: siteId,
               IsPost: 0
           }

           //Fill in NoItem with AutoNumber
           for (i = 0; i < $scope.DataDetail.length; i++) {
               $scope.DataDetail[i].PODetLine = i + 1;
           }

           var ReturnData = {
               ReturnHeader: $scope.ReturnHeaderData,
               ReturnDetail: $scope.DataDetail
           };

           var apiRoute = baseUrl + '/PostData';
           var EncReturnData = UtilityService.AESEncrypt(ReturnData);
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncReturnData));
           promise.then(function (response) {
               //let pONo = $scope.DTA.PONo;
              
               $rootScope.isLoading = false;
               $scope.EditMode = false;
               $scope.AddMode = false;
               //$scope.DTA.SPPNo = sPPNo;
               
               $scope.Data.push($scope.ReturnHeaderData);
               toastr.success('Data Saved successfully !');


           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
               toastr.error('Data Not Save !');

           });
       }
       $scope.DataDetail = [];
     

       $scope.addToList = function () {
           let RequestNo = $scope.RequestNo;
           let RequestDetLine = $scope.RequestDetLine;

           var promise = CrudService.getAll(baseUrl + '/SelectDataFromSearch/?id=' + RequestNo + '&line=' + RequestDetLine);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               var SelectedDataReq = angular.fromJson(decData);
               
               $scope.DataDetail.push({
                   PONo: $scope.DTA.PONo,
                   PODetLine: 0,
                   RequestNo: $scope.RequestNo,
                   RequestDetLine: $scope.RequestDetLine,
                   ItemID: SelectedDataReq.ItemID,
                   Price: $scope.Price,
                   ItemName: SelectedDataReq.ItemName,
                   RequestQty: SelectedDataReq.RequestQty,
                   RequestDesc: SelectedDataReq.RequestDesc
               });
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.printDocument = function (printSectionId) {
           var innerContents = document.getElementById(printSectionId).innerHTML;
           var popupWinindow = window.open('', '_blank', 'width=600,height=900,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
           popupWinindow.document.open();
           popupWinindow.document.write('<html><head> <link rel="stylesheet" type="text/css" href="Scripts/customCss/PrintDoc.css"> </head><body onload="window.print()">' + innerContents + '</html>');
           popupWinindow.document.close();
       }

       $scope.SelectReqDetData = function (idx) {
           let RequestNo = $scope.RequestDetPostData[idx].RequestNo;
           let RequestDetLine = $scope.RequestDetPostData[idx].RequestDetLine;

           var promise = CrudService.getAll(baseUrl + '/SelectDataFromSearch/?id=' + RequestNo + '&line=' + RequestDetLine);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               var SelectedData = angular.fromJson(decData);
               $scope.RequestNo = SelectedData.RequestNo;
               $scope.RequestDetLine = SelectedData.RequestDetLine;
               $scope.ItemID = SelectedData.ItemID;
               $scope.Price = '';
               $scope.RequestQty = SelectedData.RequestQty;
               $("#popUpModal").modal("hide");
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.GetSearchData = function () {
           var promise = CrudService.getAll(baseUrl + '/GetRequestDetPost');
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.RequestDetPostData = angular.fromJson(decData);
               $("#popUpModal").modal("show");
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       }

       
       $scope.getVendorName = function (vendorid) {
           
           for (i = 0; i < $scope.VendorList.length; i++)
               if ($scope.VendorList[i].VendorID === vendorid)
                   $scope.VendorNameData = $scope.VendorList[i].VendorName;

       }

      


       //Auto Generate Return No
       $scope.GenerateAutoNumber = function () {
           var promise = CrudService.getAll(baseUrl + '/genPONo');
           $scope.isLoadingNumber = true;
           promise.then(function (response) {
               $scope.DTA.PONo = response.data;
           });
       }
      

       $scope.UpdateData = function () {

           var poDate;
           var poDateSplit = $('#PODate').val().split("/");
           poDate = new Date(Date.parse(poDateSplit[2] + "-" + poDateSplit[1] + "-" + poDateSplit[0])).toUTCString();

           if (poDate != "Invalid PO Date")
               $scope.DTA.PODate = poDate;
           else
               $scope.DTA.PODate = null;

           var dlDate;
           var dlDateSplit = $('#PODeadline').val().split("/");
           dlDate = new Date(Date.parse(dlDateSplit[2] + "-" + dlDateSplit[1] + "-" + dlDateSplit[0])).toUTCString();

           if (dlDate != "Invalid PO Deadline Date")
               $scope.DTA.PODeadline = dlDate;
           else
               $scope.DTA.PODeadline = null;
          
           $scope.ReturnHeaderData = {
               PONo: $scope.DTA.PONo,
               PODate: $scope.DTA.PODate,
               PODeadline: $scope.DTA.PODeadline,
               PONotes: $scope.DTA.PONotes,
               VendorID: $scope.DTA.VendorID,
               UpdateBy: $rootScope.UserID,
               UpdateTime: '',
               SiteID: siteId,
               IsPost: $scope.DTA.IsPost
           }

           //Fill in NoItem with AutoNumber
           for (i = 0; i < $scope.DataDetail.length; i++) {
               $scope.DataDetail[i].PODetLine = i + 1;
           }

           var ReturnData = {
               ReturnHeader: $scope.ReturnHeaderData,
               ReturnDetail: $scope.DataDetail
           };

           var apiRoute = baseUrl + '/UpdateData';
           var EncReturnData = UtilityService.AESEncrypt(ReturnData);
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncReturnData));
           promise.then(function (response) {
               toastr.success('Data updated successfully !');
               $rootScope.isLoading = false;
               $scope.isUpdate = true;
               $scope.EditMode = false;
               $scope.AddMode = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
               toastr.error('Data not update !');
           });
       }

       $scope.EditData = function (keyValue) {
           
           for (i = 0; i < $scope.Data.length; i++) {
               if ($scope.Data[i].PONo === keyValue)
                   $scope.DTA = $scope.Data[i];
           }

           $("#PODate").val($.datepicker.formatDate('dd/mm/yy', new Date($scope.DTA.PODate))).trigger('change');
           $("#PODeadline").val($.datepicker.formatDate('dd/mm/yy', new Date($scope.DTA.PODeadline))).trigger('change');

           $scope.getVendorName($scope.DTA.VendorID);
           $scope.EditMode = true;
           $scope.isUpdate = true;
           $scope.GetDetail(keyValue);
       }

       $scope.EditDetailData = function (keyValue) {
           $scope.EditDetailMode = true;
           $scope.EditMode = false;
           
           for (i = 0; i < $scope.DataDetail.length; i++)
               if ($scope.DataDetail[i].RequestDetLine === keyValue)
                   $scope.DTADT = $scope.DataDetail[i];
          
        }

       $scope.AddNew = function () {
           $scope.AddMode = true;
           $scope.emptyData();
           $scope.GenerateAutoNumber();
           $scope.DataDetail = [];
           $scope.isUpdate = false;
       }

    
       $scope.Cancel = function () {
           $scope.AddMode = false;
           $scope.EditMode = false;
       }

       $scope.GetItemList = function () {
           var apiRoute = baseUrl + '/GetItemList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.ItemList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetItemList();

       $scope.GetVendorList = function () {
           var apiRoute = baseUrl + '/GetVendorList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.VendorList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetVendorList();

       $scope.GetUnitList = function () {
           var apiRoute = baseUrl + '/GetUnitList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.UnitList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetUnitList();

       $scope.getPO = function () {
           var apiRoute = baseUrl + '/GetPO/' + siteId;
           console.log(apiRoute);
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.Data = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get PO data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.getPO();

       $scope.GetDetail = function (keyValue) {
           var apiRoute = baseUrl + '/GetPODetail/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.DataDetail = angular.fromJson(decData);
               var total = 0;
               for (i = 0; i < $scope.DataDetail.length; i++) {
                   total += $scope.DataDetail[i].RequestQty * $scope.DataDetail[i].Price;
               }
               $scope.totalAmount = total;
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get PO Detail data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       

       $scope.DeleteConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this data ?"
           }).done(function () {
               $scope.DeleteData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.DeleteData = function (keyValue) {
           var apiRoute = baseUrl + '/DeletePO/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.Data.length; i++)
                   if ($scope.Data[i].RequestNo === keyValue) $scope.Data.splice(i, 1);
               toastr.success('Data deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };

       $scope.PostingConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to post this data ?"
           }).done(function () {
               $scope.PostingData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.PostingData = function (keyValue) {
           var apiRoute = baseUrl + '/PostingPO/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute);
           promise.then(function (response) {
               for (i = 0; i < $scope.Data.length; i++) {
                   if ($scope.Data[i].PONo === keyValue) {
                       $scope.Data[i].IsPost = true;
                   }
               }
               $rootScope.isLoading = false;
               toastr.success('Data posted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Post data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };


       $scope.DeleteDetailConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this detail data ?"
           }).done(function () {
               $scope.DeleteDetailData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.deleteDetail = function (index) {
           $scope.DataDetail.splice(index, 1);
       }

      

       $scope.gridOptions = {
           data: 'Data',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 20,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [
                 { name: 'PO No', field: 'PONo', enableFiltering: true, enableSorting: true, width: '40%', headerCellClass: 'text-center' },
                 { name: 'PO Date', field: 'PODate', type: 'date', width: '30%', cellFilter: 'date:\'dd-MM-yyyy\'', enableFiltering: true, enableSorting: true, headerCellClass: 'text-center' },
                 {
                     name: 'Action',
                     headerCellClass: 'text-center',
                     cellTemplate: 'actButton.html',
                     width: '30%'
                 }],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };
   }])