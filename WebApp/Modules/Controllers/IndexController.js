﻿InventoryApp.controller('IndexController', ['$scope', '$rootScope', 'accountService', '$location', '$rootScope', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService',
   function ($scope, $rootScope, accountService, $location, $rootScope, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService) {

       $scope.Title = 'Index Controller';
       $rootScope.isDebugMode = true;
       $rootScope.isAdmin = true;

       var baseUrl = serviceBasePath + '/api/Admin';

       authData.authenticationData.userName = localStorage.userName;
       authData.authenticationData.IsAuthenticated = JSON.parse(localStorage.getItem("IsAuthenticated"));
       authData.authenticationData.fullName = localStorage.fullName;

       if (!JSON.parse(localStorage.getItem("IsAuthenticated") )) {
           // sessionStorage.returnUrl = $location.url();
           $rootScope.IsAuthenticated = false;
           $location.path('/Login');
       } else $rootScope.IsAuthenticated = true;

       if (localStorage.getItem("IsPOP") != null) {
           $rootScope.IsPOP = JSON.parse(localStorage.IsPOP);
       }
       if (localStorage.getItem("IsSEM") != null) {
           $rootScope.IsSEM = JSON.parse(localStorage.IsSEM);
       }

       if (localStorage.getItem("UserID") != null) {
           $rootScope.UserID = JSON.parse(localStorage.UserID);
       }
       if (localStorage.getItem("SiteID") != null) {
           $rootScope.SiteID = JSON.parse(localStorage.SiteID);
       }

     
       $rootScope.isKollaps = true;
       $scope.Kollaps = function () {
           $rootScope.isKollaps = !$rootScope.isKollaps;
           console.log('isKolap = ' + $rootScope.isKollaps);
       }

       $("#myMenu").trigger("click");

       $scope.authentication = authData.authenticationData;
       //$location.path('/Login');
       //EnableDisableParentMenu(false);
       //if (localStorage.getItem("AccessMenu") != null) {
       //    EnableDisableParentMenu(true);
       //    var MenuItems = JSON.parse(atob(localStorage.MenuItems));
       //    for (i = 0; i < MenuItems.length; i++) {
       //        EnableDisableMenu(MenuItems[i].MenuItemTag, false);
       //    }

       //    var MenuItems = JSON.parse(atob(localStorage.AccessMenu));
       //    for (i = 0; i < MenuItems.length; i++) {
       //        EnableDisableMenu(MenuItems[i].MenuItemTag, MenuItems[i].Active);
       //    }
       //}

       $scope.GetAESKey = function () {
           var apiRoute = baseUrl + '/GetAESKey';
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var AESKey = angular.fromJson(response.data);
               $rootScope.iv = CryptoJS.enc.Utf8.parse(AESKey.iv);
               $rootScope.base64Key = CryptoJS.enc.Utf8.parse(AESKey.base64Key);
           },
           function (error) {
               if ($rootScope.isDebugMode)
                   console.error("Error: " + error);
           });
       }
       $scope.GetAESKey();
       authData.authenticationData.userName = localStorage.userName;
       authData.authenticationData.IsAuthenticated = localStorage.IsAuthenticated;
       authData.authenticationData.fullName = localStorage.fullName;

   }])