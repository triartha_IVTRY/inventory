﻿InventoryApp.controller('RequestController', ['$scope', '$rootScope', 'accountService', '$location', '$rootScope', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService', 'uiGridConstants', 'uiGridGroupingConstants',
   function ($scope, $rootScope, accountService, $location, $rootScope, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService, uiGridConstants, uiGridGroupingConstants) {
       $scope.authentication = authData.authenticationData;

       var baseUrl = serviceBasePath + '/api/Request';
       var vm = this;

       $scope.Title = 'Request';
       $scope.EditMode = false;
       $scope.AddMode = false;
       $scope.EditDetailMode = false;
       $scope.isUpdate = false;
       $rootScope.isLoading = true;
       //$rootScope.SiteID = 2;
       var siteId = $rootScope.SiteID;
       $scope.mySelections = [];
     
       $.datetimepicker.setLocale('en');
       $('#RequestDate').datetimepicker({
           yearOffset: 0,
           lang: 'ch',
           timepicker: false,
           startDate: new Date(),
           format: 'd/m/Y'
       });


       $scope.authentication = authData.authenticationData;
       if (!$scope.authentication.IsAuthenticated) {
           sessionStorage.returnUrl = $location.url();
       }
       $scope.emptyData = function () {
           $scope.DTA = {};
       }

       $scope.GetSearchData = function () {
           var promise = CrudService.getAll(baseUrl + '/GetMaterialPost');
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.MaterialPostData = angular.fromJson(decData);
               $("#popUpModal").modal("show");
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       }

       $scope.SaveData = function () {
           if ($scope.isUpdate) $scope.UpdateData();
           else $scope.SaveNewData();
           $scope.isUpdate = false;
       }

       $scope.SaveNewData = function () {

           var rqDate;
           var rqDateSplit = $('#RequestDate').val().split("/");
           rqDate = new Date(Date.parse(rqDateSplit[2] + "-" + rqDateSplit[1] + "-" + rqDateSplit[0])).toUTCString();

           if (rqDate != "Invalid Request Date")
               $scope.DTA.RequestDate = rqDate;
           else
               $scope.DTA.RequestDate = null;

           var RequestDate = $scope.DTA.RequestDate;
           var RequestDateFinal = new moment(RequestDate);

           $scope.ReturnHeaderData = {
               RequestNo: $scope.DTA.RequestNo,
               RequestDate: RequestDateFinal,
               UserID: $scope.DTA.UserID,
               ProjectID: $scope.DTA.ProjectID,
               SiteID: siteId
           }

           //Fill in NoItem with AutoNumber
           for (i = 0; i < $scope.DataDetail.length; i++) {
               $scope.DataDetail[i].RequestDetLine = i + 1;
           }

           var ReturnData = {
               ReturnHeader: $scope.ReturnHeaderData,
               ReturnDetail: $scope.DataDetail
           };


           var apiRoute = baseUrl + '/PostData';
           var EncReturnData = UtilityService.AESEncrypt(ReturnData);
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncReturnData));
           promise.then(function (response) {
               let requestNo = $scope.DTA.RequestNo;
               toastr.success('Data Saved successfully !');
               $rootScope.isLoading = false;
               $scope.EditMode = false;
               $scope.AddMode = false;
               $scope.DTA.RequestNo = requestNo;
               $scope.Data.push($scope.DTA);
           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
               showNotify('error', 'Save data failed', 'top left');
           });
       }
       $scope.DataDetail = [];


       $scope.addToList = function (key) {
           var apiRoute = baseUrl + '/GetMaterialDetail/' + key;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.MaterialDetail = angular.fromJson(decData);
               $scope.DataDetail.push({
                   RequestNo: $scope.DTA.RequestNo,
                   ItemID: $scope.MaterialDetail[0].ItemID,
                   RequestDetLine: 0,
                   ItemName: $scope.MaterialDetail[0].ItemName,
                   RequestQty: '',
                   RequestDesc: ''
               });
               $("#popUpModal").modal("hide");
           });
       }


       //Auto Generate Return No
       $scope.GenerateAutoNumber = function () {
           var promise = CrudService.getAll(baseUrl + '/genRequestNo');
           $scope.isLoadingNumber = true;
           promise.then(function (response) {
               $scope.DTA.RequestNo = response.data;
           });
       }
      

       $scope.UpdateData = function () {

           var rqDate;
           var rqDateSplit = $('#RequestDate').val().split("/");
           rqDate = new Date(Date.parse(rqDateSplit[2] + "-" + rqDateSplit[1] + "-" + rqDateSplit[0])).toUTCString();

           if (rqDate != "Invalid Request Date")
               $scope.DTA.RequestDate = rqDate;
           else
               $scope.DTA.RequestDate = null;

           var RequestDate = $scope.DTA.RequestDate;
           var RequestDateFinal = new moment(RequestDate);
   
           $scope.ReturnHeaderData = {
               RequestNo: $scope.DTA.RequestNo,
               RequestDate: RequestDateFinal.format('MM-DD-YYYY'),
               UserID: $scope.DTA.UserID,
               ProjectID: $scope.DTA.ProjectID,
               SiteID: siteId
           }

           //Fill in NoItem with AutoNumber
           for (i = 0; i < $scope.DataDetail.length; i++) {
               $scope.DataDetail[i].RequestDetLine = i + 1;
           }

           var ReturnData = {
               ReturnHeader: $scope.ReturnHeaderData,
               ReturnDetail: $scope.DataDetail
           };

           var apiRoute = baseUrl + '/UpdateData';
           var EncReturnData = UtilityService.AESEncrypt(ReturnData);
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncReturnData));
           promise.then(function (response) {
               toastr.success('Data updated successfully !');
               $rootScope.isLoading = false;
               $scope.isUpdate = true;
               $scope.EditMode = false;
               $scope.AddMode = false;

           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
               toastr.error('Data not update !');
           });
       }

       $scope.EditData = function (keyValue) {
           
          for (i = 0; i < $scope.Data.length; i++)
               if ($scope.Data[i].RequestNo === keyValue)
                   $scope.DTA = $scope.Data[i];

           $("#RequestDate").val($.datepicker.formatDate('dd/mm/yy', new Date($scope.DTA.RequestDate))).trigger('change');
           $scope.EditMode = true;
           $scope.isUpdate = true;
           $scope.GetDetail(keyValue);
       }

       $scope.EditDetailData = function (keyValue) {
           $scope.EditDetailMode = true;
           $scope.EditMode = false;
           
           for (i = 0; i < $scope.DataDetail.length; i++)
               if ($scope.DataDetail[i].RequestDetLine === keyValue)
                   $scope.DTADT = $scope.DataDetail[i];
          
        }

       $scope.AddNew = function () {
           $scope.AddMode = true;
           $scope.emptyData();
           $scope.GenerateAutoNumber();
           $scope.DataDetail = [];
       }

       $scope.SaveDetail = function () {

           var id = $scope.DTADT.RequestDetLine;
           DataClient = $scope.DTADT;
           var EncData = UtilityService.AESEncrypt(DataClient);
           $rootScope.isLoading = true;
           if ($scope.EditDetailMode) {
               var apiRoute = baseUrl + '/PutDetRequest/' + id;
               var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));
           } else if ($scope.AddMode) {
               var apiRoute = baseUrl + '/SaveRequest';
               var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncData));
           }
           promise.then(function (response) {
               let requestDetLine = response.data.RequestDetLine;
               if ($scope.EditDetailMode) {
                   for (i = 0; i < $scope.DataDetail.length; i++)
                       if ($scope.DataDetail[i].RequestDetLine === id) $scope.DataDetail.splice(i, 1);
               }
               if ($scope.AddMode) $scope.function.RequestDetLine = requestDetLine;
               $scope.DataDetail.push($scope.function);
               $rootScope.isLoading = false;
               $scope.AddMode = false;
               $scope.EditMode = false;
              
               $scope.emptyData();
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Save data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.Cancel = function () {
           $scope.AddMode = false;
           $scope.EditMode = false;
       }

       $scope.GetUserList = function () {
           var apiRoute = baseUrl + '/GetUserList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.UserList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetUserList();

       $scope.GetProjectList = function () {
           var apiRoute = baseUrl + '/GetProjectList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.ProjectList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetProjectList();

       $scope.getRequest = function () {
          var apiRoute = baseUrl + '/GetRequest/' + siteId;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.Data = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get Request data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.getRequest();

       $scope.GetDetail = function (keyValue) {
           var apiRoute = baseUrl + '/GetRequestDetail/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.DataDetail = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get Request data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       

       $scope.DeleteConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this data ?"
           }).done(function () {
               $scope.DeleteData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.DeleteData = function (keyValue) {
           var apiRoute = baseUrl + '/DeleteRequest/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.Data.length; i++)
                   if ($scope.Data[i].RequestNo === keyValue) $scope.Data.splice(i, 1);
               toastr.success('Data deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };


       $scope.DeleteDetailConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this detail data ?"
           }).done(function () {
               $scope.DeleteDetailData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.deleteDetail = function (index) {
           $scope.DataDetail.splice(index, 1);
       }

       $scope.DeleteDetailData = function (keyValue) {
           var apiRoute = baseUrl + '/DeleteDetailRequest/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.DataDetail.length; i++)
                   if ($scope.DataDetail[i].RequestDetLine === keyValue) $scope.DataDetail.splice(i, 1);
               toastr.success('Data Detail deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };

       $scope.PostingConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to post this data ?"
           }).done(function () {
               $scope.PostingData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.PostingData = function (keyValue) {
           var apiRoute = baseUrl + '/PostingRequest/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute);
           promise.then(function (response) {
               for (i = 0; i < $scope.Data.length; i++) {
                   if ($scope.Data[i].RequestNo === keyValue) {
                       $scope.Data[i].IsPost = true;
                   }
               }
               $rootScope.isLoading = false;
               toastr.success('Data posted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Post data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };


       $scope.gridOptions = {
           data: 'Data',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 20,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [
                 { name: 'Request No', field: 'RequestNo', enableFiltering: true, enableSorting: true, width: '40%', headerCellClass: 'text-center' },
                 { name: 'Request Date', field: 'RequestDate', type: 'date', cellFilter: 'date:\'dd-MM-yyyy\'', enableFiltering: true, enableSorting: true, width: '40%', headerCellClass: 'text-center' },
                 {
                     name: 'Action',
                     headerCellClass: 'text-center',
                     cellTemplate: 'actButton.html',
                     width: '20%'
                 }],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };
   }])