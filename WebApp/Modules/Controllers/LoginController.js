﻿InventoryApp.controller('LoginController', ['$scope', '$rootScope', 'accountService', '$location', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService',
   function ($scope, $rootScope, accountService, $location, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService) {
      // $scope.authentication = authData.authenticationData;
       var baseUrl = serviceBasePath + '/api/Account';
       $scope.Title = 'Login Controller';
       $rootScope.IsAuthenticated = false;
       $rootScope.isKollaps = true;
       if (!$rootScope.isKollaps)
           $("#myMenu").trigger("click");

       $scope.login = function () {
           let userName = $scope.userName;
           var promise = CrudService.getAll(baseUrl + '/GetUser/' + userName);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               var userData = angular.fromJson(decData);
               let UserPass = '';
               if (userData.length > 0) {
                   UserPass = userData[0].UserPass;
                   let password = b64_md5($scope.userName + $scope.Password);

                   if (password == UserPass) {
                       if (!$rootScope.isKollaps) $("#myMenu").trigger("click");
                       $rootScope.isKollaps = false;
                       $rootScope.IsPOP = userData.IsPOP;
                       $rootScope.IsSEM = userData.IsSEM;
                       $rootScope.IsAuthenticated = true;
                       localStorage.UserID = userData[0].UserID;
                       localStorage.SiteID = userData[0].SiteID;
                       localStorage.IsAuthenticated = true;
                       $rootScope.isKollaps = false;
                       $location.path('/Company');
                   }
                   else $scope.message = 'Password is incorrect';
               } else $scope.message = 'Username is not found !';
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       }

       $scope.Xlogin = function () {
           if ($scope.userName == 'admin' && $scope.Password == 'admin') {
               if (!$rootScope.isKollaps) $("#myMenu").trigger("click");
               $rootScope.isKollaps = false;
               $rootScope.IsPOP = true;
               $rootScope.IsSEM = true;
               $location.path('/Company');
           }
           else if ($scope.userName == 'adminpop' && $scope.Password == 'adminpop') {
               if (!$rootScope.isKollaps) $("#myMenu").trigger("click");
               $rootScope.isKollaps = false;
               $rootScope.IsPOP = true;
               $rootScope.IsSEM = false;
               localStorage.IsPOP = true;
               localStorage.IsSEM = false;
               $location.path('/Company');
           }
           else if ($scope.userName == 'adminsem' && $scope.Password == 'adminsem') {
               if (!$rootScope.isKollaps) $("#myMenu").trigger("click");
               $rootScope.isKollaps = false;
               $rootScope.IsPOP = false;
               $rootScope.IsSEM = true;
               localStorage.IsPOP = false;
               localStorage.IsSEM = true;
               $location.path('/Company');
           }
           else
               $scope.message = 'Username or password is incorrect';
       }
   }])