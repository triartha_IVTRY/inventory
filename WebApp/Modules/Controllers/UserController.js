﻿InventoryApp.controller('UserController', ['$scope', '$rootScope', 'accountService', '$location', '$rootScope', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService', 'uiGridConstants', 'uiGridGroupingConstants',
   function ($scope, $rootScope, accountService, $location, $rootScope, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService, uiGridConstants, uiGridGroupingConstants) {
       $scope.authentication = authData.authenticationData;

       var baseUrl = serviceBasePath + '/api/User';
       var vm = this;

       $scope.Title = 'User Management';
       $scope.EditMode = false;
       $scope.AddMode = false;
       $rootScope.isLoading = true;
       $scope.mySelections = [];

       $scope.authentication = authData.authenticationData;
       if (!$scope.authentication.IsAuthenticated) {
           sessionStorage.returnUrl = $location.url();
       }

       $scope.emptyData = function () {
           $scope.DTA = {};
       }
   
       $scope.getUserList = function () {
           var apiRoute = baseUrl + '/GetUserList';
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decryptedData = UtilityService.AESDecrypt(response.data);
               $scope.UserList = angular.fromJson(decryptedData); 
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + error);
           });
       }
       $scope.getUserList();

       $scope.EditData = function (keyValue) {

            for (i = 0; i < $scope.Data.length; i++)
                if ($scope.Data[i].UserID === keyValue)
                    $scope.DTA = $scope.Data[i];
                    $scope.DTA.TemporaryPass = $scope.DTA.UserPass;
                    $scope.OriginalPassword = $scope.DTA.UserPass;
                    $scope.EditMode = true;

       }

       $scope.UpdateData = function () {
           var UserData = $scope.DTA;
           if ($scope.DTA.UserPass != $scope.DTA.TemporaryPass){ 
               toastr.error('Save data failed, Password Not Match !', 'Error');
               return;
           }

           var userPass = $scope.DTA.UserPass;
           if ($scope.OriginalPassword != userPass){
               UserData.UserPass = b64_md5(UserData.UserName + UserData.UserPass);
           }   
           
           var apiRoute = baseUrl + '/UpdateData';
           var encryptedData = UtilityService.AESEncrypt(UserData);
           $rootScope.isLoading = true;
           var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(encryptedData));
           promise.then(function () {
               $scope.AddMode = false;
               $scope.EditMode = false;
               $scope.DTA = {};
               toastr.success('Update data Success', 'Success');
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.log('Update user failed. ' + error.statusText);
           });
       }

       $scope.AddNew = function () {
           $scope.AddMode = true;
           $scope.emptyData();
       }

     

       $scope.Save = function () {
            var id = $scope.DTA.UserID;
            var realPass = $scope.DTA.UserPass;
            var tmpPass = b64_md5(realPass);
            var DataClient = {
                UserName: $scope.DTA.UserName,
                UserFullName: $scope.DTA.UserFullName,
                UserPass: b64_md5(realPass),
                TemporaryPass: tmpPass,
                UserEmail: $scope.DTA.UserEmail,
                UserTypeID: $scope.DTA.UserTypeID,
                Active: $scope.DTA.Active,
                RememberMe: $scope.DTA.RememberMe
            }
            if (realPass != $scope.DTA.TemporaryPass){
                toastr.error('Save data failed, Password Not Match !', 'Error');
                return;
            }

           var EncData = UtilityService.AESEncrypt(DataClient);
           $rootScope.isLoading = true;
           var apiRoute = baseUrl + '/SaveUser';
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncData));
        
           promise.then(function (response) {
               let userID = response.data.UserID;
               $scope.DTA.UserID = userID;
               $scope.Data.push($scope.DTA);
               $rootScope.isLoading = false;
               $scope.AddMode = false;
               $scope.EditMode = false;

               $scope.emptyData();
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Save data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.Cancel = function () {
           $scope.AddMode = false;
           $scope.EditMode = false;
       }

       $scope.getUser = function () {
           var apiRoute = baseUrl + '/GetUser';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.Data = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get Administrator data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.getUser();

       $scope.DeleteConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this data ?"
           }).done(function () {
               $scope.DeleteData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.DeleteData = function (keyValue) {
           var apiRoute = baseUrl + '/DeleteUser/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.Data.length; i++)
                   if ($scope.Data[i].UserID === keyValue) $scope.Data.splice(i, 1);
               toastr.success('Data deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };

       $scope.GetUserType = function () {
           var apiRoute = baseUrl + '/GetUserType';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.UserTypeList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetUserType();

       $scope.GetSiteList = function () {
           var apiRoute = baseUrl + '/GetSiteList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.SiteList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetSiteList();

       $scope.GetDeptList = function () {
           var apiRoute = baseUrl + '/GetDeptList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.DeptList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetDeptList();

       $scope.gridOptions = {
           data: 'Data',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 20,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [
                 { name: 'User ID', field: 'UserID', "visible": false, enableFiltering: true, enableSorting: true,  headerCellClass: 'text-center' },
                
                 { name: 'User Name', field: 'UserName', enableFiltering: true, enableSorting: true, width: '25%', headerCellClass: 'text-center' },
                 { name: 'User Full Name', field: 'UserFullName', enableFiltering: true, enableSorting: true, width: '30%', headerCellClass: 'text-center' },
                 { name: 'Email', field: 'UserEmail', enableFiltering: true, enableSorting: true, width: '25%', headerCellClass: 'text-center' },
                 {
                     name: 'Action',
                     headerCellClass: 'text-center',
                     cellTemplate: 'actButton.html',
                     width: '20%'
                 }],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };
   }])