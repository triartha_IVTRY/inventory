﻿InventoryApp.controller('UnitController', ['$scope', '$rootScope', 'accountService', '$location', '$rootScope', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService', 'uiGridConstants', 'uiGridGroupingConstants',
   function ($scope, $rootScope, accountService, $location, $rootScope, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService, uiGridConstants, uiGridGroupingConstants) {
       $scope.authentication = authData.authenticationData;

       var baseUrl = serviceBasePath + '/api/Unit';
       var vm = this;

       $scope.Title = 'Unit';
       $scope.EditMode = false;
       $scope.AddMode = false;
       $rootScope.isLoading = true;
       $scope.mySelections = [];

       $scope.authentication = authData.authenticationData;
       if (!$scope.authentication.IsAuthenticated) {
           sessionStorage.returnUrl = $location.url();
       }

       $scope.emptyData = function () {
           $scope.DTA = {};
       }

       $scope.EditData = function (keyValue) {
           for (i = 0; i < $scope.Data.length; i++)
               if ($scope.Data[i].UnitID === keyValue)
                   $scope.DTA = $scope.Data[i];
           $scope.EditMode = true;
       }

       $scope.AddNew = function () {
           $scope.AddMode = true;
           $scope.emptyData();
       }

       $scope.Save = function () {
           var id = $scope.DTA.UnitID;
           DataClient = $scope.DTA;
           var EncData = UtilityService.AESEncrypt(DataClient);
           $rootScope.isLoading = true;
           if ($scope.EditMode) {
               var apiRoute = baseUrl + '/PutUnit/' + id;
               var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));
           } else if ($scope.AddMode) {
               var apiRoute = baseUrl + '/SaveUnit';
               var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncData));
           }
           promise.then(function (response) {
               let unitID = response.data.UnitID;
               if ($scope.EditMode) {
                   for (i = 0; i < $scope.Data.length; i++)
                       if ($scope.Data[i].UnitID === id) $scope.Data.splice(i, 1);
               }
               if ($scope.AddMode) $scope.DTA.UnitID = unitID;
               $scope.Data.push($scope.DTA);
               $rootScope.isLoading = false;
               $scope.AddMode = false;
               $scope.EditMode = false;

               $scope.emptyData();
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Save data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.Cancel = function () {
           $scope.AddMode = false;
           $scope.EditMode = false;
       }

       $scope.getUnit = function () {
           var apiRoute = baseUrl + '/GetUnit';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.Data = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get Unit data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.getUnit();

       $scope.DeleteConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this data ?"
           }).done(function () {
               $scope.DeleteData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.DeleteData = function (keyValue) {
           var apiRoute = baseUrl + '/DeleteUnit/' + keyValue;
            $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.Data.length; i++)
                   if ($scope.Data[i].UnitID === keyValue) $scope.Data.splice(i, 1);
               toastr.success('Data deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };


       $scope.gridOptions = {
           data: 'Data',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 20,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [
                 { name: 'Unit ID', field: 'UnitID', "visible": false, enableFiltering: true, enableSorting: true, headerCellClass: 'text-center' },
                 { name: 'Unit Short Name', field: 'UnitShortName', enableFiltering: true, enableSorting: true, width: '30%', headerCellClass: 'text-center' },
                 { name: 'Unit Name', field: 'UnitName', enableFiltering: true, enableSorting: true, width: '40%', headerCellClass: 'text-center' },
                 {
                     name: 'Action',
                     headerCellClass: 'text-center',
                     cellTemplate: 'actButton.html',
                     width: '30%'
                 }],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };
   }])