﻿InventoryApp.controller('SPPController', ['$scope', '$rootScope', 'accountService', '$location', '$rootScope', 'authData', 'CrudService', 'serviceBasePath', 'AuthServerBasePath', 'UtilityService', 'uiGridConstants', 'uiGridGroupingConstants',
   function ($scope, $rootScope, accountService, $location, $rootScope, authData, CrudService, serviceBasePath, AuthServerBasePath, UtilityService, uiGridConstants, uiGridGroupingConstants) {
       $scope.authentication = authData.authenticationData;

       var baseUrl = serviceBasePath + '/api/SPP';
       var vm = this;

       $scope.Title = 'Manajemen SPP';
       $scope.EditMode = false;
       $scope.AddMode = false;
       $scope.isUpdate = false;
       $rootScope.isLoading = true;
       $scope.mySelections = [];
       //$rootScope.SiteID = 2;
       var siteId = $rootScope.SiteID;
     
       $scope.authentication = authData.authenticationData;
       if (!$scope.authentication.IsAuthenticated) {
           sessionStorage.returnUrl = $location.url();
       }
       $scope.emptyData = function () {
           $scope.DTA = {};
       }

       $scope.GetSearchData = function () {
           var promise = CrudService.getAll(baseUrl + '/GetRequestPost');
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.RequestPostData = angular.fromJson(decData);
               $("#popUpModal").modal("show");
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       }

       $scope.SaveData = function () {
           if ($scope.isUpdate) $scope.UpdateData();
           else $scope.SaveNewData();
           $scope.isUpdate = false;
       }

       $scope.SaveNewData = function () {
          
           $scope.ReturnHeaderData = {
               SPPNo: $scope.DTA.SPPNo,
               SPPNote: $scope.DTA.SPPNote,
               UserID: $scope.DTA.UserID,
               SiteID: siteId
           }

           //Fill in NoItem with AutoNumber
           for (i = 0; i < $scope.DataDetail.length; i++) {
               $scope.DataDetail[i].SPPDetLine = i + 1;
           }

           var ReturnData = {
               ReturnHeader: $scope.ReturnHeaderData,
               ReturnDetail: $scope.DataDetail
           };


           var apiRoute = baseUrl + '/PostData';
           var EncReturnData = UtilityService.AESEncrypt(ReturnData);
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncReturnData));
           promise.then(function (response) {

               let sPPNo = $scope.DTA.SPPNo;
               toastr.success('Data Saved successfully !');
               $rootScope.isLoading = false;
               $scope.EditMode = false;
               $scope.AddMode = false;
               $scope.isUpdate = false;
               $scope.DTA.SPPNo = sPPNo;
               $scope.Data.push($scope.DTA);


           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
               showNotify('error', 'Save data failed', 'top left');
           });
       }
       $scope.DataDetail = [];


       $scope.deleteDetail = function (index) {
           $scope.DataDetail.splice(index, 1);
       }

       //Auto Generate Return No
       $scope.GenerateAutoNumber = function () {
           var promise = CrudService.getAll(baseUrl + '/genSPPNo');
           $scope.isLoadingNumber = true;
           promise.then(function (response) {
               $scope.DTA.SPPNo = response.data;
           });
       }

       $scope.addToList = function (key) {
           var apiRoute = baseUrl + '/GetRequestDetail/' + key;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.RequestDataDetail = angular.fromJson(decData);
               $scope.DataDetail.push({
                   SPPNo: $scope.DTA.SPPNo,
                   ProjectID: $scope.RequestDataDetail[0].ProjectID,
                   ProjectName: $scope.RequestDataDetail[0].ProjectName,
                   SPPDetLine: 0,
                   RequestNo: key
               });
               $("#popUpModal").modal("hide");
           });
       }

       $scope.printDocument = function (printSectionId) {
           var innerContents = document.getElementById(printSectionId).innerHTML;
           var popupWinindow = window.open('', '_blank', 'width=600,height=900,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
           popupWinindow.document.open();
           popupWinindow.document.write('<html><head> <link rel="stylesheet" type="text/css" href="Scripts/customCss/PrintDoc.css"> </head><body onload="window.print()">' + innerContents + '</html>');
           popupWinindow.document.close();
       }
      
      
       $scope.UpdateData = function () {
          
           $scope.ReturnHeaderData = {
               SPPNo: $scope.DTA.SPPNo,
               SPPNote: $scope.DTA.SPPNote,
               UserID: $scope.DTA.UserID,
               SiteID: siteId
           }
        
           for (i = 0; i < $scope.DataDetail.length; i++) {
               $scope.DataDetail[i].SPPDetLine = i + 1;
           }

           var ReturnData = {
               ReturnHeader: $scope.ReturnHeaderData,
               ReturnDetail: $scope.DataDetail
           };

           var apiRoute = baseUrl + '/UpdateData';
           var EncReturnData = UtilityService.AESEncrypt(ReturnData);
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncReturnData));
           promise.then(function (response) {
               toastr.success('Data updated successfully !');
               $rootScope.isLoading = false;
               $scope.isUpdate = true;
               $scope.EditMode = false;
               $scope.AddMode = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
               toastr.error('Data not update !');
           });
       }

       $scope.EditData = function (keyValue) {
           for (i = 0; i < $scope.Data.length; i++)
               if ($scope.Data[i].SPPNo === keyValue)
                   $scope.DTA = $scope.Data[i];
           $scope.EditMode = true;
           $scope.isUpdate = true;
           $scope.GetDetail(keyValue);
           $scope.GetDetailRequest(keyValue);
       }

       $scope.EditDetailData = function (keyValue) {
           $scope.EditDetailMode = true;
           $scope.EditMode = false;
           
           for (i = 0; i < $scope.DataDetail.length; i++)
               if ($scope.DataDetail[i].RequestDetLine === keyValue)
                   $scope.DTADT = $scope.DataDetail[i];
          
        }

       $scope.AddNew = function () {
           $scope.AddMode = true;
           $scope.emptyData();
           $scope.GenerateAutoNumber();
           $scope.DataDetail = [];
       }

       $scope.Save = function () {
           var id = $scope.DTA.SPPNo;
           DataClient = $scope.DTA;
           var EncData = UtilityService.AESEncrypt(DataClient);
           $rootScope.isLoading = true;
           if ($scope.EditMode) {
               var apiRoute = baseUrl + '/PutSPP/' + id;
               var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));
           } else if ($scope.AddMode) {
               var apiRoute = baseUrl + '/SaveSPP';
               var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncData));
           }
           promise.then(function (response) {
               let sppNo = response.data.SPPNo;
               if ($scope.EditMode) {
                   for (i = 0; i < $scope.Data.length; i++)
                       if ($scope.Data[i].SPPNo === id) $scope.Data.splice(i, 1);
               }
               if ($scope.AddMode) $scope.DTA.SPPNo = sppNo;
               $scope.Data.push($scope.DTA);
               $rootScope.isLoading = false;
               $scope.AddMode = false;
               $scope.EditMode = false;

               $scope.emptyData();
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Save data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.SaveDetail = function () {

           var id = $scope.DTADT.SPPDetLine;
           DataClient = $scope.DTADT;
           var EncData = UtilityService.AESEncrypt(DataClient);
           $rootScope.isLoading = true;
           if ($scope.EditDetailMode) {
               var apiRoute = baseUrl + '/PutDetSPP/' + id;
               var promise = CrudService.put(apiRoute, UtilityService.QuotedStr(EncData));
           } else if ($scope.AddMode) {
               var apiRoute = baseUrl + '/SaveDetSPP';
               var promise = CrudService.post(apiRoute, UtilityService.QuotedStr(EncData));
           }
           promise.then(function (response) {
               let sppDetLine = response.data.SPPDetLine;
               if ($scope.EditDetailMode) {
                   for (i = 0; i < $scope.DataDetail.length; i++)
                       if ($scope.DataDetail[i].SPPDetLine === id) $scope.DataDetail.splice(i, 1);
               }
               if ($scope.AddMode) $scope.function.SPPDetLine = sppDetLine;
               $scope.DataDetail.push($scope.function);
               $rootScope.isLoading = false;
               $scope.AddMode = false;
               $scope.EditMode = false;

               $scope.emptyData();
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Save data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });

       }

       $scope.Cancel = function () {
           $scope.AddMode = false;
           $scope.EditMode = false;
       }

       $scope.GetUserList = function () {
           var apiRoute = baseUrl + '/GetUserList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.UserList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetUserList();

       $scope.GetRequestList = function () {
           var apiRoute = baseUrl + '/GetRequestList';
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.RequestList = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               if ($rootScope.isDebugMode) console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.GetRequestList();

       $scope.getSPP = function () {
           var apiRoute = baseUrl + '/GetSPP/' + siteId;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.Data = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get SPP data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.getSPP();

       

       $scope.GetDetail = function (keyValue) {
           //var keyValue = keyValue.replace(/\//g, '~');

           var apiRoute = baseUrl + '/GetSPPDetail/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
              var decData = UtilityService.AESDecrypt(response.data);
              $scope.DataDetail = angular.fromJson(decData);
              $scope.CreatedDate = $scope.DataDetail[0].CreateTime;
              $rootScope.isLoading = false;

           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get SPP Detail data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };

       $scope.GetDetailRequest = function (keyValue) {
           //var keyValue = keyValue.replace(/\//g, '~'); 
           var apiRoute = baseUrl + '/GetDetailRequest/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.getAll(apiRoute);
           promise.then(function (response) {
               var decData = UtilityService.AESDecrypt(response.data);
               $scope.DataDetailReq = angular.fromJson(decData);
               $rootScope.isLoading = false;
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Can not get Detail Request data !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };
       $scope.DeleteConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this data ?"
           }).done(function () {
               $scope.DeleteData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.DeleteData = function (keyValue) {
           var apiRoute = baseUrl + '/DeleteSPP/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.Data.length; i++)
                   if ($scope.Data[i].SPPNo === keyValue) $scope.Data.splice(i, 1);
               toastr.success('Data SPP deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };

       $scope.PostingConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to post this data ?"
           }).done(function () {
               $scope.PostingData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.PostingData = function (keyValue) {
           var apiRoute = baseUrl + '/PostingSPP/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.post(apiRoute);
           promise.then(function (response) {
               for (i = 0; i < $scope.Data.length; i++) {
                   if ($scope.Data[i].SPPNo === keyValue) {
                       $scope.Data[i].IsPost = true;
                   }
               }
               $rootScope.isLoading = false;
               toastr.success('Data posted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Post data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };



       $scope.DeleteDetailConfirmation = function (keyValue) {
           $.MessageBox({
               buttonDone: "Yes",
               buttonFail: "No",
               message: "Are you sure want to delete this detail data ?"
           }).done(function () {
               $scope.DeleteDetailData(keyValue);
               $scope.$apply();
           }).fail(function () {
               return;
           });
       }

       $scope.DeleteDetailData = function (keyValue) {
           var apiRoute = baseUrl + '/DeleteDetailSPP/' + keyValue;
           $rootScope.isLoading = true;
           var promise = CrudService.delete(apiRoute);
           promise.then(function (response) {
               $rootScope.isLoading = false;
               for (i = 0; i < $scope.DataDetail.length; i++)
                   if ($scope.DataDetail[i].SPPDetLine === keyValue) $scope.DataDetail.splice(i, 1);
               toastr.success('Data Detail SPP deleted successfully !');
           },
           function (error) {
               $rootScope.isLoading = false;
               toastr.error('Delete data failed !', 'Error');
               if ($rootScope.isDebugMode)
                   console.error("Error: " + JSON.stringify(error));
           });
       };


       $scope.gridOptions = {
           data: 'Data',
           enableCellEditOnFocus: true,
           enableColumnResizing: true,
           enableFiltering: true,
           enableGridMenu: true,
           enableSelectAll: true,
           fastWatch: true,
           paginationPageSizes: [5, 10, 20, 50, 100],
           paginationPageSize: 20,
           exporterCsvFilename: 'ExportFile.csv',
           exporterPdfDefaultStyle: { fontSize: 9 },
           exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
           exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
           exporterPdfHeader: { text: "Header", style: 'headerStyle' },
           exporterPdfFooter: function (currentPage, pageCount) {
               return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
           },
           exporterPdfCustomFormatter: function (docDefinition) {
               docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
               docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
               return docDefinition;
           },
           exporterPdfOrientation: 'portrait',
           exporterPdfPageSize: 'LETTER',
           exporterPdfMaxGridWidth: 500,
           exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
           exporterExcelFilename: 'ExportFile.xlsx',
           exporterExcelSheetName: 'Sheet1',
           columnDefs: [
                 { name: 'SPP No', field: 'SPPNo', enableFiltering: true, enableSorting: true, width: '70%', headerCellClass: 'text-center' },
                 {
                     name: 'Action',
                     headerCellClass: 'text-center',
                     cellTemplate: 'actButton.html',
                     width: '30%'
                 }],
           onRegisterApi: function (gridApi) {
               $scope.gridApi = gridApi;
           }
       };
   }])