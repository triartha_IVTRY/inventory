﻿InventoryApp.service('UtilityService',['$rootScope', function ($rootScope) {

    this.AESEncrypt = function (JsonData) {
        var strJsonData = JSON.stringify(JsonData);

        var EncData = CryptoJS.AES.encrypt(strJsonData,
                          $rootScope.base64Key,
                          {
                              iv: $rootScope.iv,
                              padding: CryptoJS.pad.Pkcs7,
                              mode: CryptoJS.mode.CBC
                          });
        return EncData;
    }

    this.AESDecrypt = function (ecncryptData) {
        var cipherParams = CryptoJS.lib.CipherParams.create({
            ciphertext: CryptoJS.enc.Base64.parse(ecncryptData)
        });
        var decrypted = CryptoJS.AES.decrypt(cipherParams,
                             $rootScope.base64Key,
                             {
                                 iv: $rootScope.iv,
                                 padding: CryptoJS.pad.Pkcs7,
                                 mode: CryptoJS.mode.CBC
                             });

        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    this.QuotedStr = function (str) {
        return '"' + str + '"';
    }


}]);


