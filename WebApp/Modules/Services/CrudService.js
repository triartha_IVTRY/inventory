﻿InventoryApp.service('CrudService',['$http', function ($http) {

    var urlGet = '';
    this.post = function (apiRoute, Model) {
        var request = $http({
            method: "post",
            url: apiRoute,
            data: Model
        });
        return request;
    }
    this.put = function (apiRoute, Model) {
        var request = $http({
            method: "put",
            url: apiRoute,
            data: Model
        });
        return request;
    }
    this.delete = function (apiRoute) {
        var request = $http({
            method: "delete",
            url: apiRoute
        });
        return request;
    }
    this.getAll = function (apiRoute) {
        urlGet = apiRoute;
        return $http.get(urlGet);
    }

    this.getWithParam = function (apiRoute, Model) { 
        return $http.get(apiRoute, {params : Model});
    }

    this.getbyID = function (apiRoute, dataID) {

        urlGet = apiRoute + '/' + dataID;
        return $http.get(urlGet);
    }

    this.uploadFile = function (url, file) {
        return $http({
            url: url,
            method: 'POST',
            data: file,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        });
    }

}]);


