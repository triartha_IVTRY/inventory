﻿InventoryApp.config(['$httpProvider', function ($httpProvider) {
    var interceptor = function (userService, $q, $location) {        
        return {           
            request: function (config) {
                var currentUser = userService.GetCurrentUser();
                if (currentUser != null) {
                    config.headers['Authorization'] = 'Bearer ' + currentUser.access_token;
                }

                return config;
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    //var $http = injector.get('$http'); 
                    //$.post("http://localhost:25419/token/" + "refresh_token=" + sessionStorage.refresh_token + "&grant_type=refresh_token", function (response) {
                    //});

                    $location.path('/Login');
                    return $q.reject(rejection);
                }
                if (rejection.status === 403) {
                    $location.path('/Unauthorized');
                    return $q.reject(rejection);
                }
                return $q.reject(rejection);
            }

        }
    }
 
    var params = ['userService', '$q', '$location'];
    interceptor.$inject = params;
    $httpProvider.interceptors.push(interceptor);
}]);