﻿var InventoryApp = angular.module('InventoryApp', ['ngRoute', 'ngTouch', 'angularjs-crypto', 'ui.grid', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.pinning', 'ui.grid.selection', 'ui.grid.moveColumns', 'ui.grid.exporter', 'ui.grid.importer', 'ui.grid.grouping', 'ui.grid.saveState', ]);
//var ArthaApp = angular.module('ArthaApp', []);
InventoryApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
    .when('/', {
        redirectTo: '/Company',
    })

    .when('/Login', {
        templateUrl: 'modules/Views/Login.view.html',
        controller: 'LoginController'
    })
    .when('/Home', {
        templateUrl: 'modules/Views/Home.view.html',
        controller: 'HomeController'
    })

    .when('/Register', {
        templateUrl: 'modules/Views/Register.view.html',
        controller: 'RegisterController'
    })

    .when('/AboutUs', {
        templateUrl: 'modules/Views/AboutUs.view.html'
    })

   
    .when('/Company', {
        templateUrl: 'modules/Views/Company.view.html',
        controller: 'CompanyController'
    })

     .when('/Site', {
         templateUrl: 'modules/Views/Site.view.html',
         controller: 'SiteController'
     })

     .when('/Dept', {
         templateUrl: 'modules/Views/Dept.view.html',
         controller: 'DeptController'
     })

    .when('/Unit', {
        templateUrl: 'modules/Views/Unit.view.html',
        controller: 'UnitController'
    })

     .when('/Item', {
         templateUrl: 'modules/Views/Item.view.html',
         controller: 'ItemController'
     })

    .when('/Vendor', {
        templateUrl: 'modules/Views/Vendor.view.html',
        controller: 'VendorController'
     })

     .when('/User', {
         templateUrl: 'modules/Views/User.view.html',
         controller: 'UserController'
     })

      .when('/Project', {
          templateUrl: 'modules/Views/Project.view.html',
          controller: 'ProjectController'
      })

      .when('/Request', {
           templateUrl: 'modules/Views/Request.view.html',
           controller: 'RequestController'
      })

     .when('/SPP', {
         templateUrl: 'modules/Views/SPP.view.html',
         controller: 'SPPController'
     })

     .when('/PO', {
         templateUrl: 'modules/Views/PO.view.html',
         controller: 'POController'
     })

    .when('/ApprovalSPP', {
        templateUrl: 'modules/Views/ApprovalSPP.view.html',
        controller: 'ApprovalSPPController'
    })

    .when('/ApprovalSPPDetail/:sppid', {
        templateUrl: 'modules/Views/ApprovalSPPDetail.view.html',
        controller: 'ApprovalSPPDetailController'
    })


}])

InventoryApp.constant('serviceBasePath', 'http://localhost:56001');
InventoryApp.constant('AuthServerBasePath', 'http://localhost:56002');